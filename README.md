
# Tools to create and install new `Panoptes` releases.


## Install the tools.

Copy the content of the `create` directory to a destination directory in your `PATH` variable, to make these commands available.

Copy the content of the `install` directory to `/group/rich/bin` at the pit, and make sure that `/group/rich/bin` is in your `PATH` variable.

All the tools are scripts, so nothing has to be compiled.


## Create a new `Panoptes` release.

Run `mkpanoptes` to prepare a new `Panoptes` release in the current working directory. It is advised to run it on `lxplus`.

The script checks out the content of the repository, compiles the requested branch for different platforms and runs tests.

You can follow the instructions from the script to tag the new release, send the project to the release builds and check the build progress.


## Install a new `Panoptes` release at the pit.

From the online system, run `installpanoptes` to install the new version and follow the instructions. This will create a new release directory in `/group/rich/sw/cmtuser`, but will not make it the default release in use. The online system executes the monitoring scripts in `/group/rich/scripts`, which are soft links to the `Panoptes` release currently in use.

Find a good time with the piquet to make the latest available release the default.

